# Module piveau-model

THE pipe model

# Package io.piveau.pipe

Convenient management classes

# Package io.piveau.pipe.model

The actual data model for the pipe

# Package io.piveau.pipe.validation

Some validation helper