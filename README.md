# edp pipe model

## Table of Contents
1. [Build & Install](#build-and-install)
1. [Use](#use)
1. [License](#license)

## Build and Install
Requirements:
 * Git
 * Maven
 * Java

```bash
$ git clone https://gitlab.com/european-data-portal/pipe/edp-pipe-model.git
$ cd edp-pipe-model
$ mvn install
```

## Use
Add dependency to your project pom file:
```xml
<dependency>
    <groupId>io.piveau.pipe</groupId>
    <artifactId>pipe-model</artifactId>
    <version>4.0.0</version>
</dependency>
```

## License

[Apache License, Version 2.0](LICENSE)
